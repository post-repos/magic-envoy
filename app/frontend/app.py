from flask import Flask, abort
import json
import socket
import urllib.request

app = Flask(__name__)


@app.route('/')
def index():
    return json.dumps({
        'hostname': socket.gethostname()
        })

@app.route('/healtz')
def healtz():
    return "OK"

@app.route('/service1')
def service1():
    try:
        contents = urllib.request.urlopen("http://localhost:666").read()
        contents = {"message":str(contents)}
        return app.response_class(
            response=json.dumps(contents),
            mimetype='application/json'
        )
    except Exception as e:
        print("[ERROR]: {}".format(e))
        abort(503)


@app.route('/service2')
def service2():
    try:
        contents = urllib.request.urlopen("http://localhost:777").read()
        contents = {"message":str(contents)}
        return app.response_class(
            response=json.dumps(contents),
            mimetype='application/json'
        )
    except Exception as e:
        print("[ERROR]: {}".format(e))
        abort(503)


@app.errorhandler(503)
def page_not_found(e):
    return app.response_class(
        response=json.dumps('{"code":"503","message":"internal error"'),
        mimetype='application/json'
    )

@app.errorhandler(404)
def page_not_found(e):
    return app.response_class(
        response=json.dumps('{"code":"404","message":"page not found"'),
        mimetype='application/json'
    )

if __name__ == '__main__':
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
    app.run(host='0.0.0.0', port=80, debug=True)

