# Doing some magic with Envoy, the wizard...

# Summary

Put an already working docker set of services, listening in different ports in the host's network, to work in Kubernetes.

_This repo is related to [this post](https://juanmatiasdelacamara.wordpress.com/?p=663) in my blog._

## What we have

Ok, let's say we have an app. It is split into a few services. These services are already dockerized.

These services are being deployed using docker compose, share the same network: _host_; and listen in different ports. So, each one of them that wants to contact one of the other services, just needs to hit the right port in _127.0.0.1_.

Let's put it in a draw:

![Dockerized Solution](./images/dockerized.png)

Let's say that if _frontend_ want to talk to _service0_, it will talk to _127.0.0.1:666_. If _service0_ want to talk to _service1_ will talk to _127.0.0.1:777_, and so on.

## The constraints

For reasons that are not relevant to this article, these are the constraints:

  - We can not modify the code
  - so, we can not modify the services' behavior
  - this means, services will continue hitting _127.0.0.1:n_ to reach other services

## The problem with K8s

Since we want to put each service in a separate pod (this way we can handle scalability, resilience, etc), each service will end up with a different IP number.

How can we have different IP in each pod but still allow the services to reach each other on _127.0.0.1:n_?

## Envoy magic

[Envoy Proxy](https://www.envoyproxy.io/) is an open source edge and service proxy, designed for cloud-native applications.

Basically, we will let envoy listen in the ports other than the current service's, and redirect the traffic to the desired target.

The schema:

![Kubernetes Solution](./images/k8s.png)

In the example (the red-wine line), _frontend_ receives a request, and to answer it, _frontend_ needs first to contact _service1_. _frontend_ doesn't know it is in a Kubernete's pod, so it tries to contact the service at _127.0.0.1:777_. Then, _envoy_ receives the request and, applying rules, redirects the traffic to the _service1_'s service. (remember in front of a K8s' pod there is a service)

Then, the same will happen if _service1_ needs to contact any of the other services.

## Let's do it

We will be playing here with three components:

  - a _frontend_
  - a service called _service1_
  - a service called _service2_

_service1_ will listen in port _666_ and _service2_ will listen in _777_.

In this simple example, only _frontend_ will try to connect to services, and it will do it on _127.0.0.1:n_. (then, you can figure out how to implement envoy in all the pods)

You can dig into the code, it's just Python.

The _frontend_ will query the services and return what they returned.

The services are just two _go_ programs, that will listen in a port and will return a string of text.

The code for _frontend_ is included in this repo. The code for services is from [this](https://juanmatiasdelacamara.wordpress.com/2019/03/14/simple-canary-with-affinity-on-k8s/) work.

Anyway, the docker images are already in _dockerhub_, so they can be used directly.

### Deploy'em all

Ok, in this repo there is also a set of charts. (under _helmcharts_) 

  - Frontend
  - Service1
  - Service2

In the _frontend_'s _deployment.yaml_ file, it can be seen that the deployment has two containers. One for the actual app (frontend) and one for _envoy_. This last container has mounted as a volume a secret with the envoy's settings.

Let's dig into these settings.

```
apiVersion: v1
kind: Secret
metadata:
  name: {{ include "frontend.fullname" . }}
  labels:
    {{- include "frontend.labels" . | nindent 4 }}
type: Opaque
stringData:
  envoy.yaml: |-
    admin:
      access_log_path: /tmp/admin_access.log
      address:
        socket_address: { address: 127.0.0.1, port_value: 9901 }
    
    static_resources:
      listeners:
      - name: listener_0
        address:
          socket_address: { address: 127.0.0.1, port_value: 666 }
        filter_chains:
        - filters:
          - name: envoy.filters.network.http_connection_manager
            typed_config:
              "@type": type.googleapis.com/envoy.config.filter.network.http_connection_manager.v2.HttpConnectionManager
              stat_prefix: ingress_http
              codec_type: AUTO
              route_config:
                name: local_route
                virtual_hosts:
                - name: local_service
                  domains: ["*"]
                  routes:
                  - match: { prefix: "/" }
                    route: { cluster: service1 }
              http_filters:
              - name: envoy.filters.http.router
                typed_config: {}
      - name: listener_1
        address:
          socket_address: { address: 127.0.0.1, port_value: 777 }
        filter_chains:
        - filters:
          - name: envoy.filters.network.http_connection_manager
            typed_config:
              "@type": type.googleapis.com/envoy.config.filter.network.http_connection_manager.v2.HttpConnectionManager
              stat_prefix: ingress_http
              codec_type: AUTO
              route_config:
                name: local_route
                virtual_hosts:
                - name: local_service
                  domains: ["*"]
                  routes:
                  - match: { prefix: "/" }
                    route: { cluster: service2 }
              http_filters:
              - name: envoy.filters.http.router
                typed_config: {}
      clusters:
      - name: service1
        connect_timeout: 0.25s
        type: STRICT_DNS
        lb_policy: ROUND_ROBIN
        load_assignment:
          cluster_name: service1
          endpoints:
          - lb_endpoints:
            - endpoint:
                address:
                  socket_address:
                    address: service1
                    port_value: 666
      - name: service2
        connect_timeout: 0.25s
        type: STRICT_DNS
        lb_policy: ROUND_ROBIN
        load_assignment:
          cluster_name: service2
          endpoints:
          - lb_endpoints:
            - endpoint:
                address:
                  socket_address:
                    address: service2
                    port_value: 777

```

As can be seen, there are _listeners_ and _clusters_. The _envoy proxy_ will listen in those _listeners_ and redirect the traffic, following rules, to the _clusters_.

It's pretty simple, so I'll let you learn from this code.

So, let's deploy'em all... (it is assumed that you already have a cluster)

We will use a namespace called `envoy-magic`:

```
cd helmcharts
helm3 install service1 -n envoy-magic -f service1/values.yaml service1
helm3 install service2 -n envoy-magic -f service2/values.yaml service2
```

So far, we have the two deployments, each one with its own Kubernte's service in front of it, let's check it:

```
╰─ kubectl get svc -n envoy-magic
NAME       TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
service1   ClusterIP   10.12.9.214   <none>        666/TCP   3m25s
service2   ClusterIP   10.12.3.200   <none>        777/TCP   2m43
```

IPs can change, see the ports.

Now, deploy the frontend.

```
helm3 install frontend -n envoy-magic -f frontend/values.yaml frontend 
```

After a minute or so, we'll have an ingress up and running:

```
╰─ kgi -n envoy-magic 
NAME       HOSTS   ADDRESS          PORTS   AGE
frontend   *       34.102.230.249   80      12m
```

### Test the solution

Now, let's try it. Hit its address:

```
╰─ curl 34.102.230.249         
{"hostname": "frontend-7c7986dfc9-hx9vg"}
```

_hostname will vary, it's the name of your node._

This is the root path of the server just returning the _hostname_, if you dig into the frontend code:

```
@app.route('/')
def index():
    return json.dumps({
        'hostname': socket.gethostname()
        })
```

But then we have two more endpoints, one for hitting _service1_ another one for _service2_.

```
@app.route('/service1')
def service1():
    try:
        contents = urllib.request.urlopen("http://localhost:666").read()
        contents = {"message":str(contents)}
        return app.response_class(
            response=json.dumps(contents),
            mimetype='application/json'
        )
    except Exception as e:
        print("[ERROR]: {}".format(e))
        abort(503)
```

Note it is hitting _localhost:666_. For _service2_ it will be hitting _localhost:777_.

Try them:

```
╰─ curl 34.102.230.249/service1
{"message": "b'Congratulations! Version 1.0 of your application is running on Kubernetes.'"}

╰─ curl 34.102.230.249/service2
{"message": "b'Congratulations! Version 2.0 of your application is running on Kubernetes.'"}
```

## Conclusion

If you have a legacy app, listening in a port and hitting other ports on _localhost_.

If you can't modify the code for whatever reason.

If you need to put this solution to work on Kubernetes.


You still have the chance of do it with _envoy_. (or any other similar proxy)


![Proxies](./images/proxies.jpg)
